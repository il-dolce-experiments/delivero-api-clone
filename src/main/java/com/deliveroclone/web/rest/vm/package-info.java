/**
 * View Models used by Spring MVC REST controllers.
 */
package com.deliveroclone.web.rest.vm;
